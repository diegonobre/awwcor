<?php

namespace Tests\AppBundle\Controller\Api;

use AppBundle\Entity\Comment;
use Draw\Bundle\DrawTestHelperBundle\Helper\WebTestCaseTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CommentControllerTest extends WebTestCase
{
    use WebTestCaseTrait;

    public function testListAction()
    {
        return $this->requestHelper()
            ->get('/api/posts/2/comments')
            ->jsonHelper()
            ->propertyHelper('')->assertCount(5)->end()
            ->executeAndJsonDecode();
    }
}

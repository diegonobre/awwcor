UPDATE user u
  JOIN (SELECT (CASE WHEN max(ur.updated_at) IS NOT NULL
                     THEN max(ur.updated_at)
                     ELSE u2.updated_at END
               ) AS new_date
               ,u2.id
          FROM user u2
          LEFT JOIN user_recipient ur ON ur.account_owner_id = u2.id
         GROUP BY u2.updated_at
       ) AS user_date ON user_date.id = u.id
   SET u.lastActivityDate = user_date.new_date;
